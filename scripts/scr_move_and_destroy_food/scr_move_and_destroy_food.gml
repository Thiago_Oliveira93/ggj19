///@arg shef_origin

var shelf = argument0

depth = obj_food_origin_1.depth-1

if place_meeting(x,y,obj_chef_hand)
{
	if obj_chef_hand.l_click
	{
		x = obj_chef_hand.x
		y = obj_chef_hand.y
	}
}

if place_meeting(x,y,obj_no_food_zone) and !obj_chef_hand.l_click
{
		instance_destroy()
}