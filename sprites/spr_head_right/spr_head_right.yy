{
    "id": "9a6fcd3a-0808-403d-80b7-5387e8aa5300",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_head_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 183,
    "bbox_left": 106,
    "bbox_right": 178,
    "bbox_top": 100,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fce76829-6014-4ded-8754-194186d2bf92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a6fcd3a-0808-403d-80b7-5387e8aa5300",
            "compositeImage": {
                "id": "1ffee3e3-9d88-4e5b-a276-187734a282b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fce76829-6014-4ded-8754-194186d2bf92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d20778e-3d8d-4afe-96a2-f3be6de99d7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fce76829-6014-4ded-8754-194186d2bf92",
                    "LayerId": "1ee85a23-7a16-48d5-b020-f56579ff174c"
                }
            ]
        },
        {
            "id": "a3cfdddb-6d66-4827-9fa4-5ed4243dfeb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a6fcd3a-0808-403d-80b7-5387e8aa5300",
            "compositeImage": {
                "id": "6b00ea6f-5d97-4b73-95c3-c748beb97c7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3cfdddb-6d66-4827-9fa4-5ed4243dfeb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3055c9f1-7d44-4e9a-bf38-d6dbcc57a8f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3cfdddb-6d66-4827-9fa4-5ed4243dfeb9",
                    "LayerId": "1ee85a23-7a16-48d5-b020-f56579ff174c"
                }
            ]
        },
        {
            "id": "162394b2-33dd-45a8-898b-51f554521817",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a6fcd3a-0808-403d-80b7-5387e8aa5300",
            "compositeImage": {
                "id": "fadc5bbd-a944-45db-a39f-7a171b8bca50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "162394b2-33dd-45a8-898b-51f554521817",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e7e847a-da14-4aa5-884e-42662428f2cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "162394b2-33dd-45a8-898b-51f554521817",
                    "LayerId": "1ee85a23-7a16-48d5-b020-f56579ff174c"
                }
            ]
        },
        {
            "id": "fc050fd5-5b1d-4683-9764-81ac36a0df15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a6fcd3a-0808-403d-80b7-5387e8aa5300",
            "compositeImage": {
                "id": "ab92ae68-9fd2-4a31-bee7-1960ee2b44c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc050fd5-5b1d-4683-9764-81ac36a0df15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f62d7550-9ce6-4e6f-9547-007da0ab850e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc050fd5-5b1d-4683-9764-81ac36a0df15",
                    "LayerId": "1ee85a23-7a16-48d5-b020-f56579ff174c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 284,
    "layers": [
        {
            "id": "1ee85a23-7a16-48d5-b020-f56579ff174c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9a6fcd3a-0808-403d-80b7-5387e8aa5300",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 284,
    "xorig": 142,
    "yorig": 142
}