{
    "id": "ff96b2a1-85d3-42ca-b4cd-1cab57491f1e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_body_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 223,
    "bbox_left": 101,
    "bbox_right": 182,
    "bbox_top": 61,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f7310c7e-700f-4994-b9a1-8eba9b6a06aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff96b2a1-85d3-42ca-b4cd-1cab57491f1e",
            "compositeImage": {
                "id": "c0aa55c4-2fb0-4910-8972-e70ac3cdc3b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7310c7e-700f-4994-b9a1-8eba9b6a06aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60450386-1da2-472a-8a12-2c910a44a1a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7310c7e-700f-4994-b9a1-8eba9b6a06aa",
                    "LayerId": "e01acd5a-babe-4fff-bb82-3749bdc1f933"
                }
            ]
        },
        {
            "id": "91e75bbb-a310-4b4f-a632-53478136321d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff96b2a1-85d3-42ca-b4cd-1cab57491f1e",
            "compositeImage": {
                "id": "07f1f7b2-e482-415c-8a6a-28e19bf00e83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91e75bbb-a310-4b4f-a632-53478136321d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "add8ca5f-f52f-4060-9ee8-6ec8bc82820c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91e75bbb-a310-4b4f-a632-53478136321d",
                    "LayerId": "e01acd5a-babe-4fff-bb82-3749bdc1f933"
                }
            ]
        },
        {
            "id": "27dea336-b6bc-4d12-9f77-4adab01fdee5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff96b2a1-85d3-42ca-b4cd-1cab57491f1e",
            "compositeImage": {
                "id": "5db45fb4-e5a3-4f59-85f0-23c78a0bb2e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27dea336-b6bc-4d12-9f77-4adab01fdee5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e73ecae0-836f-4986-a521-1f53ecaed763",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27dea336-b6bc-4d12-9f77-4adab01fdee5",
                    "LayerId": "e01acd5a-babe-4fff-bb82-3749bdc1f933"
                }
            ]
        },
        {
            "id": "a45df327-e3b6-4d19-94ba-98de4fbb8af7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff96b2a1-85d3-42ca-b4cd-1cab57491f1e",
            "compositeImage": {
                "id": "ecfcea2b-c21b-4275-aec6-56862f24cf2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a45df327-e3b6-4d19-94ba-98de4fbb8af7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e85680a5-4e59-4669-ba5e-fa5f1ca61abe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a45df327-e3b6-4d19-94ba-98de4fbb8af7",
                    "LayerId": "e01acd5a-babe-4fff-bb82-3749bdc1f933"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 285,
    "layers": [
        {
            "id": "e01acd5a-babe-4fff-bb82-3749bdc1f933",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ff96b2a1-85d3-42ca-b4cd-1cab57491f1e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 284,
    "xorig": 142,
    "yorig": 142
}