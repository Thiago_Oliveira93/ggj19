{
    "id": "2edcce5c-1b0f-41a8-88e3-27b7c602051f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ico1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 200,
    "bbox_left": 75,
    "bbox_right": 208,
    "bbox_top": 83,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b20be227-7211-4278-a684-f048e123c551",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2edcce5c-1b0f-41a8-88e3-27b7c602051f",
            "compositeImage": {
                "id": "ed685e6a-5493-4077-9428-49a9c73d834c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b20be227-7211-4278-a684-f048e123c551",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a652d0e5-7fb7-493a-96b8-4f5803469c22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b20be227-7211-4278-a684-f048e123c551",
                    "LayerId": "c5ada6a0-33b7-409f-9ee5-e4dec86bf7e5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 284,
    "layers": [
        {
            "id": "c5ada6a0-33b7-409f-9ee5-e4dec86bf7e5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2edcce5c-1b0f-41a8-88e3-27b7c602051f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 284,
    "xorig": 142,
    "yorig": 142
}