{
    "id": "3ff09728-54e1-4f1d-a4f9-26cecac7cb8a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_customer",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 120,
    "bbox_left": 29,
    "bbox_right": 43,
    "bbox_top": 35,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f0bc1b59-2603-4c26-86f5-a0b84a7281ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3ff09728-54e1-4f1d-a4f9-26cecac7cb8a",
            "compositeImage": {
                "id": "d79d6ec0-5285-4cf7-ab62-7bdc5fbfc968",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0bc1b59-2603-4c26-86f5-a0b84a7281ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee6bd77f-ef46-42eb-9363-3bc650709d09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0bc1b59-2603-4c26-86f5-a0b84a7281ac",
                    "LayerId": "706a13ac-fc95-41dc-884a-12f438d4bfe4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 140,
    "layers": [
        {
            "id": "706a13ac-fc95-41dc-884a-12f438d4bfe4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3ff09728-54e1-4f1d-a4f9-26cecac7cb8a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 72,
    "xorig": 36,
    "yorig": 70
}