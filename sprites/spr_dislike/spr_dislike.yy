{
    "id": "9ab88d66-5aa6-48ff-a0bf-4f1df7b1114b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dislike",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cb1f5528-2399-405b-add7-3c2c3837044e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ab88d66-5aa6-48ff-a0bf-4f1df7b1114b",
            "compositeImage": {
                "id": "6513cbc8-a525-4fbf-af11-478d5629fce9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb1f5528-2399-405b-add7-3c2c3837044e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70edb386-c722-4aa7-9867-75afb9bcf219",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb1f5528-2399-405b-add7-3c2c3837044e",
                    "LayerId": "7127b7e8-793e-4f63-ab19-19d91fd2e1f4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7127b7e8-793e-4f63-ab19-19d91fd2e1f4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9ab88d66-5aa6-48ff-a0bf-4f1df7b1114b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}