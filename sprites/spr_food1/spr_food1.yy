{
    "id": "20e3606a-8dca-4535-a0a1-de86a04ddb23",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_food1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 0,
    "bbox_right": 79,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "37b33db9-bf3d-49ad-ab4d-63f9b3b2fb20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20e3606a-8dca-4535-a0a1-de86a04ddb23",
            "compositeImage": {
                "id": "44e3346b-ba3b-4f08-8e8c-ce0999a8750d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37b33db9-bf3d-49ad-ab4d-63f9b3b2fb20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34fe60f3-9bf6-4839-a948-cc44cfbf1daa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37b33db9-bf3d-49ad-ab4d-63f9b3b2fb20",
                    "LayerId": "f5580df3-0206-49ba-be8d-9c54b8fb518a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "f5580df3-0206-49ba-be8d-9c54b8fb518a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "20e3606a-8dca-4535-a0a1-de86a04ddb23",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 40,
    "yorig": 40
}