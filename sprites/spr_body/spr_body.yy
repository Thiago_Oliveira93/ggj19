{
    "id": "c78c45f9-efde-46e8-ae8c-1022a7af758b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_body",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 221,
    "bbox_left": 89,
    "bbox_right": 195,
    "bbox_top": 64,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2dce5b45-91cc-44f9-a485-0ec6dd8eda17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c78c45f9-efde-46e8-ae8c-1022a7af758b",
            "compositeImage": {
                "id": "ad224000-7ba7-4c62-b8f2-a20d47049f2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2dce5b45-91cc-44f9-a485-0ec6dd8eda17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc93f995-e71d-4c98-8e80-538e4c91493f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2dce5b45-91cc-44f9-a485-0ec6dd8eda17",
                    "LayerId": "62b2721b-9531-4261-aa66-0e87034258d1"
                }
            ]
        },
        {
            "id": "b92fa75d-1eae-4c40-addf-66e8298a28c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c78c45f9-efde-46e8-ae8c-1022a7af758b",
            "compositeImage": {
                "id": "3e1aef89-f887-4f59-8916-de16080b4ee2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b92fa75d-1eae-4c40-addf-66e8298a28c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a01599ea-0baa-439e-8891-0b34ff98503f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b92fa75d-1eae-4c40-addf-66e8298a28c2",
                    "LayerId": "62b2721b-9531-4261-aa66-0e87034258d1"
                }
            ]
        },
        {
            "id": "f7f9d937-949d-4e2c-9713-f366c4fdb2b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c78c45f9-efde-46e8-ae8c-1022a7af758b",
            "compositeImage": {
                "id": "6bc0e06c-216f-4789-b820-d97ff74a5bb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7f9d937-949d-4e2c-9713-f366c4fdb2b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3398fa96-3b03-4bd0-be63-d579431a095c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7f9d937-949d-4e2c-9713-f366c4fdb2b2",
                    "LayerId": "62b2721b-9531-4261-aa66-0e87034258d1"
                }
            ]
        },
        {
            "id": "0e6b5e8f-135e-4643-96c9-c72aba729b8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c78c45f9-efde-46e8-ae8c-1022a7af758b",
            "compositeImage": {
                "id": "9a0693db-645d-4c84-8a68-0c90ca2f64ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e6b5e8f-135e-4643-96c9-c72aba729b8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5582a8c7-066a-47f1-8231-c626dfed04b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e6b5e8f-135e-4643-96c9-c72aba729b8c",
                    "LayerId": "62b2721b-9531-4261-aa66-0e87034258d1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 285,
    "layers": [
        {
            "id": "62b2721b-9531-4261-aa66-0e87034258d1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c78c45f9-efde-46e8-ae8c-1022a7af758b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 285,
    "xorig": 142,
    "yorig": 142
}