{
    "id": "c59e05b8-2436-47e2-8823-002663badd41",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_balcao",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 183,
    "bbox_left": 0,
    "bbox_right": 1365,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b237d113-dc89-4900-92ee-50de48855b3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c59e05b8-2436-47e2-8823-002663badd41",
            "compositeImage": {
                "id": "fa39ae46-74f6-4334-a3a0-f62c2c87b65d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b237d113-dc89-4900-92ee-50de48855b3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07be0706-1f16-43d9-9ac2-b471a4cd606f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b237d113-dc89-4900-92ee-50de48855b3f",
                    "LayerId": "391d00fa-7dc0-4894-b386-6570ebfcb376"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 200,
    "layers": [
        {
            "id": "391d00fa-7dc0-4894-b386-6570ebfcb376",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c59e05b8-2436-47e2-8823-002663badd41",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1366,
    "xorig": 683,
    "yorig": 100
}