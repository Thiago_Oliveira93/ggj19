{
    "id": "ca9dbcf6-0dad-44ca-b164-ff6b61bb64f3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_food3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 0,
    "bbox_right": 79,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "91f517b0-acf5-4a54-92c1-706e071e95a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca9dbcf6-0dad-44ca-b164-ff6b61bb64f3",
            "compositeImage": {
                "id": "cae3fb5c-2083-4b8b-b092-ba97757e27c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91f517b0-acf5-4a54-92c1-706e071e95a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed853eb3-f863-42a9-b0a8-ba80569a9625",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91f517b0-acf5-4a54-92c1-706e071e95a6",
                    "LayerId": "841c3c45-9b3c-4755-a704-595d6ae05086"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "841c3c45-9b3c-4755-a704-595d6ae05086",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ca9dbcf6-0dad-44ca-b164-ff6b61bb64f3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 40,
    "yorig": 40
}