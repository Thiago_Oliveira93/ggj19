{
    "id": "8b4bb6f5-e6de-4e59-b1e3-0cacc6f9329d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_food_origin2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 214,
    "bbox_left": 79,
    "bbox_right": 204,
    "bbox_top": 70,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "910b0dc4-d94f-4d41-a830-9e5dd7ff0681",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b4bb6f5-e6de-4e59-b1e3-0cacc6f9329d",
            "compositeImage": {
                "id": "800fd25a-b9fa-4928-a26f-4851cd4cc7eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "910b0dc4-d94f-4d41-a830-9e5dd7ff0681",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e96e8a0-6dc7-43d9-8dab-a59e7bd9ca9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "910b0dc4-d94f-4d41-a830-9e5dd7ff0681",
                    "LayerId": "a261618e-9b03-4960-9720-bbb19a87f9e7"
                }
            ]
        },
        {
            "id": "415d1ee8-734a-412f-abe1-b73e5b782724",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b4bb6f5-e6de-4e59-b1e3-0cacc6f9329d",
            "compositeImage": {
                "id": "a6ec4e34-2d33-4b15-8427-8b130869dc34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "415d1ee8-734a-412f-abe1-b73e5b782724",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd140e79-5b67-43b0-b4e0-4ad499adf22c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "415d1ee8-734a-412f-abe1-b73e5b782724",
                    "LayerId": "a261618e-9b03-4960-9720-bbb19a87f9e7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 285,
    "layers": [
        {
            "id": "a261618e-9b03-4960-9720-bbb19a87f9e7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8b4bb6f5-e6de-4e59-b1e3-0cacc6f9329d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 284,
    "xorig": 142,
    "yorig": 142
}