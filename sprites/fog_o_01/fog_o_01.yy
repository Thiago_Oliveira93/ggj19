{
    "id": "b1d9486e-51b9-4725-bb72-4336e8a2310b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "fog_o_01",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 204,
    "bbox_left": 84,
    "bbox_right": 201,
    "bbox_top": 79,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b5897878-104c-4dda-bb6b-ce3f4017a0e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1d9486e-51b9-4725-bb72-4336e8a2310b",
            "compositeImage": {
                "id": "937aa24c-912d-4102-8c0a-19dcb15ad955",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5897878-104c-4dda-bb6b-ce3f4017a0e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6162707-24cb-4d44-abde-516d6b7cbdbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5897878-104c-4dda-bb6b-ce3f4017a0e0",
                    "LayerId": "6591a910-0095-4430-ba0b-93a3bafad2df"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 284,
    "layers": [
        {
            "id": "6591a910-0095-4430-ba0b-93a3bafad2df",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b1d9486e-51b9-4725-bb72-4336e8a2310b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 285,
    "xorig": 84,
    "yorig": 78
}