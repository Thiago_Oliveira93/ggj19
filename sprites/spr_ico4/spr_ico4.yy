{
    "id": "1e905dd1-7397-400d-937b-43fed8ceec7d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ico4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 197,
    "bbox_left": 79,
    "bbox_right": 204,
    "bbox_top": 87,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5f5640a1-5946-4352-a295-38c3fbd2ba9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e905dd1-7397-400d-937b-43fed8ceec7d",
            "compositeImage": {
                "id": "1c54efd4-106a-4b4f-9d9d-bfa218d230d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f5640a1-5946-4352-a295-38c3fbd2ba9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb267267-a193-4aba-b62b-0d88f7674e6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f5640a1-5946-4352-a295-38c3fbd2ba9c",
                    "LayerId": "680eca3f-dcd9-48b7-a9b1-ce0bcba9fbc4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 284,
    "layers": [
        {
            "id": "680eca3f-dcd9-48b7-a9b1-ce0bcba9fbc4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1e905dd1-7397-400d-937b-43fed8ceec7d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 284,
    "xorig": 142,
    "yorig": 142
}