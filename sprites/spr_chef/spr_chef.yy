{
    "id": "d92c4b81-2d90-42d7-81a1-25f96aebd577",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_chef",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 119,
    "bbox_left": 0,
    "bbox_right": 119,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "56c7beb4-eeed-4c64-a1ee-63f5de89a111",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d92c4b81-2d90-42d7-81a1-25f96aebd577",
            "compositeImage": {
                "id": "aa6b38af-cf26-44e9-b067-961ab1002c36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56c7beb4-eeed-4c64-a1ee-63f5de89a111",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9e888d0-ef60-4ae2-9129-f5713768e905",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56c7beb4-eeed-4c64-a1ee-63f5de89a111",
                    "LayerId": "7eb0ea7a-736f-4a38-bd81-4aca87905d14"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "7eb0ea7a-736f-4a38-bd81-4aca87905d14",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d92c4b81-2d90-42d7-81a1-25f96aebd577",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 120,
    "xorig": 60,
    "yorig": 60
}