{
    "id": "d850d4e4-240d-4ae4-880d-6fc84a96fc62",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_food_origin4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 214,
    "bbox_left": 80,
    "bbox_right": 205,
    "bbox_top": 70,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "90c8855e-6af9-4147-a4b2-d665f60031e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d850d4e4-240d-4ae4-880d-6fc84a96fc62",
            "compositeImage": {
                "id": "9fc462fc-d827-433a-8496-d55aeaae02d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90c8855e-6af9-4147-a4b2-d665f60031e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "077bcceb-8bcc-43ee-9921-2971f1f1bf8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90c8855e-6af9-4147-a4b2-d665f60031e4",
                    "LayerId": "9cd13eff-8deb-4cf5-96b4-6d718802a473"
                }
            ]
        },
        {
            "id": "5cba5d24-a9e8-4a03-978c-f68a5c6e4659",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d850d4e4-240d-4ae4-880d-6fc84a96fc62",
            "compositeImage": {
                "id": "6abc5a71-e790-49d6-b0d7-96e4d1972b43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5cba5d24-a9e8-4a03-978c-f68a5c6e4659",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1beaa247-1033-480c-84b9-2f38af6be82b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5cba5d24-a9e8-4a03-978c-f68a5c6e4659",
                    "LayerId": "9cd13eff-8deb-4cf5-96b4-6d718802a473"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 285,
    "layers": [
        {
            "id": "9cd13eff-8deb-4cf5-96b4-6d718802a473",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d850d4e4-240d-4ae4-880d-6fc84a96fc62",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 285,
    "xorig": 142,
    "yorig": 142
}