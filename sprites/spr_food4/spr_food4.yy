{
    "id": "60942c54-4134-459f-9511-3bb60902d498",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_food4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 0,
    "bbox_right": 79,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "04d14f98-03b7-4c81-bb8b-9e72e1843684",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60942c54-4134-459f-9511-3bb60902d498",
            "compositeImage": {
                "id": "0ec790af-c449-4334-a776-48dac4cc2746",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04d14f98-03b7-4c81-bb8b-9e72e1843684",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86ec5a59-1519-4253-a72e-772ca87e4f9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04d14f98-03b7-4c81-bb8b-9e72e1843684",
                    "LayerId": "2999d441-d454-4402-b4bf-04dbc931f328"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "2999d441-d454-4402-b4bf-04dbc931f328",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "60942c54-4134-459f-9511-3bb60902d498",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 40,
    "yorig": 40
}