{
    "id": "749fa1ef-91b8-45f4-a4cb-3f222492959a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_like",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "59656b9f-dd71-4298-9970-f248c5377412",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "749fa1ef-91b8-45f4-a4cb-3f222492959a",
            "compositeImage": {
                "id": "eb4ff5bb-ff6a-4e81-a9a8-76e96541c772",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59656b9f-dd71-4298-9970-f248c5377412",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d706d70d-dceb-4fdb-893e-637f66ec3ef8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59656b9f-dd71-4298-9970-f248c5377412",
                    "LayerId": "1d80fb9a-5f00-4441-af00-7ab5eb121e5b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1d80fb9a-5f00-4441-af00-7ab5eb121e5b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "749fa1ef-91b8-45f4-a4cb-3f222492959a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}