{
    "id": "4fc9cfdd-1e6e-47b4-a6be-c9df0c596d0c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_next_day",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 402,
    "bbox_left": 381,
    "bbox_right": 983,
    "bbox_top": 125,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6ce06579-5396-4af1-a100-639952b156f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4fc9cfdd-1e6e-47b4-a6be-c9df0c596d0c",
            "compositeImage": {
                "id": "b176466a-4d68-40ad-9778-cbac0d44f9ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ce06579-5396-4af1-a100-639952b156f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "546303d2-3b42-42de-89fb-68daef9ce8d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ce06579-5396-4af1-a100-639952b156f2",
                    "LayerId": "755e56c3-36c0-4abf-8758-b149c2b4f624"
                }
            ]
        },
        {
            "id": "ce85c7c0-f33e-4c95-8220-aab5e7433b2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4fc9cfdd-1e6e-47b4-a6be-c9df0c596d0c",
            "compositeImage": {
                "id": "8615a8b3-40a3-4055-8a5f-252743a4e3ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce85c7c0-f33e-4c95-8220-aab5e7433b2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09642f30-9666-45b4-9b46-6fea8a37c9f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce85c7c0-f33e-4c95-8220-aab5e7433b2f",
                    "LayerId": "755e56c3-36c0-4abf-8758-b149c2b4f624"
                }
            ]
        },
        {
            "id": "d5ce28be-39c7-49c7-beed-c89358ad16f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4fc9cfdd-1e6e-47b4-a6be-c9df0c596d0c",
            "compositeImage": {
                "id": "62dd1ff8-0268-4c4e-9a81-2572806a4cb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5ce28be-39c7-49c7-beed-c89358ad16f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6068dea3-dbd2-4990-aaec-ef6f6b6e3f79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5ce28be-39c7-49c7-beed-c89358ad16f0",
                    "LayerId": "755e56c3-36c0-4abf-8758-b149c2b4f624"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "755e56c3-36c0-4abf-8758-b149c2b4f624",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4fc9cfdd-1e6e-47b4-a6be-c9df0c596d0c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1366,
    "xorig": 683,
    "yorig": 384
}