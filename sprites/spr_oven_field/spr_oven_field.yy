{
    "id": "d20162d5-d2b0-4538-84ac-32d62ae6dd3a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_oven_field",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "72ea94fa-9b28-454a-95fd-c8b53e197867",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d20162d5-d2b0-4538-84ac-32d62ae6dd3a",
            "compositeImage": {
                "id": "70bc0af6-3712-41ba-a97d-7acb67138364",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72ea94fa-9b28-454a-95fd-c8b53e197867",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8691bf4-61a6-4f71-80b1-c001ed3c8dd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72ea94fa-9b28-454a-95fd-c8b53e197867",
                    "LayerId": "b9c0d40a-927a-4b2b-ab9d-49035d84b6c4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b9c0d40a-927a-4b2b-ab9d-49035d84b6c4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d20162d5-d2b0-4538-84ac-32d62ae6dd3a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}