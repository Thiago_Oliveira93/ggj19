{
    "id": "8b353cc7-dfcf-4e2e-8f09-3360a5f70e74",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_food_origin",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 219,
    "bbox_left": 74,
    "bbox_right": 208,
    "bbox_top": 62,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "24b30eae-f0da-4104-8e9f-3ada6a99b7c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b353cc7-dfcf-4e2e-8f09-3360a5f70e74",
            "compositeImage": {
                "id": "cb58c3b3-6ba1-4cec-8b2d-72e650e0a4a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24b30eae-f0da-4104-8e9f-3ada6a99b7c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03e1242b-af12-441b-981c-417f16a517a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24b30eae-f0da-4104-8e9f-3ada6a99b7c9",
                    "LayerId": "c552f315-d0fe-4612-be30-d7b6b66fbca2"
                }
            ]
        },
        {
            "id": "c1ba3411-fe0b-46e7-9e06-f03e833c399b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b353cc7-dfcf-4e2e-8f09-3360a5f70e74",
            "compositeImage": {
                "id": "f80d74af-fb7c-40f1-852b-a73de53bc9ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1ba3411-fe0b-46e7-9e06-f03e833c399b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66601ff0-bc67-4c0a-87b0-9082ddcf8c71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1ba3411-fe0b-46e7-9e06-f03e833c399b",
                    "LayerId": "c552f315-d0fe-4612-be30-d7b6b66fbca2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 285,
    "layers": [
        {
            "id": "c552f315-d0fe-4612-be30-d7b6b66fbca2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8b353cc7-dfcf-4e2e-8f09-3360a5f70e74",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 285,
    "xorig": 142,
    "yorig": 142
}