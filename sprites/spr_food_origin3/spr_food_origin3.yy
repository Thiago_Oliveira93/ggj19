{
    "id": "962d5e1b-732b-4877-b853-91c568ee04c3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_food_origin3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 219,
    "bbox_left": 74,
    "bbox_right": 207,
    "bbox_top": 62,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4ddc6c55-5a0e-4ba2-921d-53f1f2abb8c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "962d5e1b-732b-4877-b853-91c568ee04c3",
            "compositeImage": {
                "id": "46ee0cca-9284-4243-89e7-6587aea84c4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ddc6c55-5a0e-4ba2-921d-53f1f2abb8c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "625ab766-ef8f-4fa2-a50a-46e4457ffb05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ddc6c55-5a0e-4ba2-921d-53f1f2abb8c0",
                    "LayerId": "eca937c5-8e85-4d17-b2c6-ce3bfa528f39"
                }
            ]
        },
        {
            "id": "41b0c9e6-d648-437e-9af2-36771a0d83b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "962d5e1b-732b-4877-b853-91c568ee04c3",
            "compositeImage": {
                "id": "50e33cbf-fb70-440c-8ff4-27f39e9a5de0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41b0c9e6-d648-437e-9af2-36771a0d83b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e45b4f22-dbb3-4105-8378-98438e792bf5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41b0c9e6-d648-437e-9af2-36771a0d83b5",
                    "LayerId": "eca937c5-8e85-4d17-b2c6-ce3bfa528f39"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 285,
    "layers": [
        {
            "id": "eca937c5-8e85-4d17-b2c6-ce3bfa528f39",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "962d5e1b-732b-4877-b853-91c568ee04c3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 285,
    "xorig": 142,
    "yorig": 142
}