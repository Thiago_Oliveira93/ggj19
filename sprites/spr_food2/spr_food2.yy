{
    "id": "b46bd10e-3fca-4aae-8584-cc5416614e1d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_food2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 0,
    "bbox_right": 79,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "12ef5a1a-6f61-41f9-9c88-30bce2fd67ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b46bd10e-3fca-4aae-8584-cc5416614e1d",
            "compositeImage": {
                "id": "4c78bb38-8a8b-4b0e-9743-a6037d42a2d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12ef5a1a-6f61-41f9-9c88-30bce2fd67ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04689e50-3826-4941-be9c-941b3fba0726",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12ef5a1a-6f61-41f9-9c88-30bce2fd67ed",
                    "LayerId": "80c3686d-66d3-4a52-9abc-dc1adc8760aa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "80c3686d-66d3-4a52-9abc-dc1adc8760aa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b46bd10e-3fca-4aae-8584-cc5416614e1d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 40,
    "yorig": 40
}