{
    "id": "86dce11c-7450-474e-ba0e-2840df79fca6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_plate",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 173,
    "bbox_left": 89,
    "bbox_right": 194,
    "bbox_top": 111,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2d9a4480-5299-4eba-b25d-ddb09ecb4d85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86dce11c-7450-474e-ba0e-2840df79fca6",
            "compositeImage": {
                "id": "ee52e339-a2ab-49ff-9c98-0edcee634286",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d9a4480-5299-4eba-b25d-ddb09ecb4d85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c42c2ea8-3a8d-46b3-9254-1272ce359611",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d9a4480-5299-4eba-b25d-ddb09ecb4d85",
                    "LayerId": "b1d835f1-67d1-4cd1-9349-e93df9d51a72"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 285,
    "layers": [
        {
            "id": "b1d835f1-67d1-4cd1-9349-e93df9d51a72",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "86dce11c-7450-474e-ba0e-2840df79fca6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 284,
    "xorig": 142,
    "yorig": 142
}