{
    "id": "ef1fd3af-e2a6-4752-9f43-2cc7613fe632",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pan1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 120,
    "bbox_left": 18,
    "bbox_right": 52,
    "bbox_top": 11,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4da5e056-8b03-4b1b-ac4a-4b40b033786d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef1fd3af-e2a6-4752-9f43-2cc7613fe632",
            "compositeImage": {
                "id": "9d875248-bb70-4953-aa11-09b8a68ce2cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4da5e056-8b03-4b1b-ac4a-4b40b033786d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fef7021-658c-4a5b-a901-1ef898e8b3ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4da5e056-8b03-4b1b-ac4a-4b40b033786d",
                    "LayerId": "827ed6cf-504e-4d79-a487-fd648353a2b9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 284,
    "layers": [
        {
            "id": "827ed6cf-504e-4d79-a487-fd648353a2b9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ef1fd3af-e2a6-4752-9f43-2cc7613fe632",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 285,
    "xorig": 142,
    "yorig": 142
}