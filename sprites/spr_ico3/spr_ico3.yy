{
    "id": "ac885f60-1a4f-4a1d-a813-5214ebd83329",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ico3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 197,
    "bbox_left": 74,
    "bbox_right": 208,
    "bbox_top": 86,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c85a503e-0b3f-455f-bd5e-e0567cf30555",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac885f60-1a4f-4a1d-a813-5214ebd83329",
            "compositeImage": {
                "id": "e1b3f16a-0576-43b4-9eed-2b92372ff46b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c85a503e-0b3f-455f-bd5e-e0567cf30555",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6241d8e8-6662-4e7d-be64-031b1f59c70e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c85a503e-0b3f-455f-bd5e-e0567cf30555",
                    "LayerId": "c0e8a327-65cc-4732-b48e-5fdc07926b0f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 284,
    "layers": [
        {
            "id": "c0e8a327-65cc-4732-b48e-5fdc07926b0f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ac885f60-1a4f-4a1d-a813-5214ebd83329",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 285,
    "xorig": 142,
    "yorig": 142
}