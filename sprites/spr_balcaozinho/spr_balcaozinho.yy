{
    "id": "8965a0bf-b9e4-48bc-9038-702edc3d0a5b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_balcaozinho",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 205,
    "bbox_left": 83,
    "bbox_right": 201,
    "bbox_top": 79,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "818f9e69-d9bf-412b-b4dd-19c670d69fd3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8965a0bf-b9e4-48bc-9038-702edc3d0a5b",
            "compositeImage": {
                "id": "d1714b41-2ca5-4efa-a934-6a296611923f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "818f9e69-d9bf-412b-b4dd-19c670d69fd3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c493f13c-4966-4359-96cc-420fc08dc3e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "818f9e69-d9bf-412b-b4dd-19c670d69fd3",
                    "LayerId": "ef95ca71-225f-4184-8552-e74c8a343fe1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 285,
    "layers": [
        {
            "id": "ef95ca71-225f-4184-8552-e74c8a343fe1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8965a0bf-b9e4-48bc-9038-702edc3d0a5b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 285,
    "xorig": 142,
    "yorig": 142
}