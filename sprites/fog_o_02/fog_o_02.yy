{
    "id": "852ca8b2-8096-474e-ba93-c0e78449117e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "fog_o_02",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 204,
    "bbox_left": 86,
    "bbox_right": 197,
    "bbox_top": 79,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1e1bcb52-2a4c-4b54-b978-1299b2386d5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "852ca8b2-8096-474e-ba93-c0e78449117e",
            "compositeImage": {
                "id": "2c66ffb1-d64b-4d49-b17f-d5f6e715a298",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e1bcb52-2a4c-4b54-b978-1299b2386d5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb739fc9-7c13-4374-bda4-229b4ebf6d63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e1bcb52-2a4c-4b54-b978-1299b2386d5d",
                    "LayerId": "ec7bf2b9-6ce1-4d55-b58b-30f1df4cff9b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 284,
    "layers": [
        {
            "id": "ec7bf2b9-6ce1-4d55-b58b-30f1df4cff9b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "852ca8b2-8096-474e-ba93-c0e78449117e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 284,
    "xorig": 87,
    "yorig": 79
}