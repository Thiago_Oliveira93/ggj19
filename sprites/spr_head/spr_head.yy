{
    "id": "7169e1f5-6fff-463e-8d5d-e5bc967eb920",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_head",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 183,
    "bbox_left": 86,
    "bbox_right": 197,
    "bbox_top": 101,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ee2e3edf-2b53-453f-b06a-28da9a01ec34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7169e1f5-6fff-463e-8d5d-e5bc967eb920",
            "compositeImage": {
                "id": "33ba517b-2273-405c-8067-3c3d8c1b3354",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee2e3edf-2b53-453f-b06a-28da9a01ec34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55013c0e-6e03-4f75-8a4c-042e05600870",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee2e3edf-2b53-453f-b06a-28da9a01ec34",
                    "LayerId": "d64e0993-d878-4216-8457-7d4403cc6dfa"
                }
            ]
        },
        {
            "id": "e69afd76-463c-4145-a05a-0c0b116d234d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7169e1f5-6fff-463e-8d5d-e5bc967eb920",
            "compositeImage": {
                "id": "7c85d95e-ddbd-4192-94af-1bddd24d50a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e69afd76-463c-4145-a05a-0c0b116d234d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9176c35-5988-42fe-a912-26b688eaae4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e69afd76-463c-4145-a05a-0c0b116d234d",
                    "LayerId": "d64e0993-d878-4216-8457-7d4403cc6dfa"
                }
            ]
        },
        {
            "id": "c2aa5fe3-cf7d-4b15-8d32-c2afbd0c6cd1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7169e1f5-6fff-463e-8d5d-e5bc967eb920",
            "compositeImage": {
                "id": "240f9117-bbd3-44bb-9380-e8cf4e525f2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2aa5fe3-cf7d-4b15-8d32-c2afbd0c6cd1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87e0b353-f690-4891-ad64-0cc506675be0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2aa5fe3-cf7d-4b15-8d32-c2afbd0c6cd1",
                    "LayerId": "d64e0993-d878-4216-8457-7d4403cc6dfa"
                }
            ]
        },
        {
            "id": "c0f8e1c1-35c3-470c-834f-60925da3ed62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7169e1f5-6fff-463e-8d5d-e5bc967eb920",
            "compositeImage": {
                "id": "08fde115-440e-4ac8-8e12-b8eaba1b7c25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0f8e1c1-35c3-470c-834f-60925da3ed62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4687ba79-5f50-4808-8ea4-77531d79466b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0f8e1c1-35c3-470c-834f-60925da3ed62",
                    "LayerId": "d64e0993-d878-4216-8457-7d4403cc6dfa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 284,
    "layers": [
        {
            "id": "d64e0993-d878-4216-8457-7d4403cc6dfa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7169e1f5-6fff-463e-8d5d-e5bc967eb920",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 285,
    "xorig": 142,
    "yorig": 142
}