{
    "id": "f72730cc-5fee-4c64-9acb-529e2d9ed583",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ico2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 192,
    "bbox_left": 90,
    "bbox_right": 194,
    "bbox_top": 92,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7ce26f8b-a41e-4cdf-88a0-6c714c682ff2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f72730cc-5fee-4c64-9acb-529e2d9ed583",
            "compositeImage": {
                "id": "2efad56d-3a59-4b6c-b659-e919a4040955",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ce26f8b-a41e-4cdf-88a0-6c714c682ff2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d78c9ea-092c-4b11-960e-3b71536dfa74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ce26f8b-a41e-4cdf-88a0-6c714c682ff2",
                    "LayerId": "34c6ee74-c84a-4044-99e3-df7cf65c8216"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 284,
    "layers": [
        {
            "id": "34c6ee74-c84a-4044-99e3-df7cf65c8216",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f72730cc-5fee-4c64-9acb-529e2d9ed583",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 285,
    "xorig": 142,
    "yorig": 142
}