{
    "id": "3a3cb6a2-110a-4004-9ff2-7c4a860eb6d4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite24",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 224,
    "bbox_left": 95,
    "bbox_right": 190,
    "bbox_top": 60,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "53705f34-f7b8-4bfd-b27c-9853b885945c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a3cb6a2-110a-4004-9ff2-7c4a860eb6d4",
            "compositeImage": {
                "id": "e14cce73-3802-4642-97fe-8fd0f15dfc94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53705f34-f7b8-4bfd-b27c-9853b885945c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef3093aa-5799-492d-93bd-af5ef9b02fa8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53705f34-f7b8-4bfd-b27c-9853b885945c",
                    "LayerId": "c360f606-74df-4695-b16e-0bf47906a64d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 285,
    "layers": [
        {
            "id": "c360f606-74df-4695-b16e-0bf47906a64d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3a3cb6a2-110a-4004-9ff2-7c4a860eb6d4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 285,
    "xorig": 142,
    "yorig": 142
}