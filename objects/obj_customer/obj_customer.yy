{
    "id": "d1dcb5fb-387a-4253-a1f7-27c3c2e3d7ab",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_customer",
    "eventList": [
        {
            "id": "fd4251fa-73c4-42f6-b3d2-8c511b5c8c5b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d1dcb5fb-387a-4253-a1f7-27c3c2e3d7ab"
        },
        {
            "id": "fdb22fa4-51f2-4702-b52f-f58f118b2c47",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d1dcb5fb-387a-4253-a1f7-27c3c2e3d7ab"
        },
        {
            "id": "f535ba1f-aa40-4ec5-ae81-d6c3a7040fbb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "d1dcb5fb-387a-4253-a1f7-27c3c2e3d7ab"
        },
        {
            "id": "484db597-7f47-4af3-8a6f-f3d8dc1c314c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d1dcb5fb-387a-4253-a1f7-27c3c2e3d7ab"
        },
        {
            "id": "0e5af807-21f2-4361-a2df-0a218298ea62",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "d1dcb5fb-387a-4253-a1f7-27c3c2e3d7ab"
        },
        {
            "id": "dd602e67-af42-4eba-9603-62754bb80ad0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "6d91430b-5049-44f8-9d75-9853d64e9071",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d1dcb5fb-387a-4253-a1f7-27c3c2e3d7ab"
        },
        {
            "id": "bc0d057b-1c65-445f-8da0-3132e8536606",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "d1dcb5fb-387a-4253-a1f7-27c3c2e3d7ab"
        },
        {
            "id": "0e199338-773c-4be7-bd7b-a7ea6cbbebad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "c149f667-a6bc-457f-8247-f00a9558d8bf",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d1dcb5fb-387a-4253-a1f7-27c3c2e3d7ab"
        },
        {
            "id": "c5024d1c-dde1-489c-bfc1-3d78b2c9ab29",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "d1dcb5fb-387a-4253-a1f7-27c3c2e3d7ab"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3ff09728-54e1-4f1d-a4f9-26cecac7cb8a",
    "visible": true
}