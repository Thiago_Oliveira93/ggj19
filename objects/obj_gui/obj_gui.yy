{
    "id": "3732a769-ecf5-45b2-929e-1b0ca56d8346",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_gui",
    "eventList": [
        {
            "id": "cdf004e1-363c-4968-adac-95493d434f27",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "3732a769-ecf5-45b2-929e-1b0ca56d8346"
        },
        {
            "id": "c529f0c0-a328-46ab-b249-e669fd946cc6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "3732a769-ecf5-45b2-929e-1b0ca56d8346"
        },
        {
            "id": "185c69e5-bc34-4740-b571-bdc76e900705",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3732a769-ecf5-45b2-929e-1b0ca56d8346"
        },
        {
            "id": "163a55d0-2959-416d-a9a6-4c56b2fb6ed7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "3732a769-ecf5-45b2-929e-1b0ca56d8346"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}