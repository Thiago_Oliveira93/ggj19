{
    "id": "e28464cc-af29-45aa-b3ed-d3a8d1878adb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_food_4",
    "eventList": [
        {
            "id": "61f00da6-5a56-4aa5-9917-0e93342838ef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e28464cc-af29-45aa-b3ed-d3a8d1878adb"
        },
        {
            "id": "80b8b2d2-f426-4e26-8aa2-717f03e02ee6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e28464cc-af29-45aa-b3ed-d3a8d1878adb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "aafc3e6f-ee39-4d2d-847e-c2e29d38d05a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "60942c54-4134-459f-9511-3bb60902d498",
    "visible": true
}