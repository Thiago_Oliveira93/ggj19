{
    "id": "de99d444-ce6c-42d6-8c78-2cf85d03a7ea",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_no_food_zone",
    "eventList": [
        {
            "id": "fbde4911-c0ae-4159-8cb0-2d34708b7130",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "de99d444-ce6c-42d6-8c78-2cf85d03a7ea"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e9fd309e-7555-4e99-8fc2-a5438cc3f016",
    "visible": true
}