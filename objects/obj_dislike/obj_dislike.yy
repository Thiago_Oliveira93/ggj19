{
    "id": "299bd856-f526-4820-a240-62cf321d32f9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_dislike",
    "eventList": [
        {
            "id": "cb3db93d-0f7b-471e-87ab-c30cc87258f7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "299bd856-f526-4820-a240-62cf321d32f9"
        },
        {
            "id": "b062fc96-1a6d-4a34-8353-bd54dd4ebdba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "299bd856-f526-4820-a240-62cf321d32f9"
        },
        {
            "id": "10d77d69-7da7-4722-b003-a3c7b900f03a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "299bd856-f526-4820-a240-62cf321d32f9"
        },
        {
            "id": "e8a948e8-977c-4eeb-9c54-e49fcefccea2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "299bd856-f526-4820-a240-62cf321d32f9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9ab88d66-5aa6-48ff-a0bf-4f1df7b1114b",
    "visible": true
}