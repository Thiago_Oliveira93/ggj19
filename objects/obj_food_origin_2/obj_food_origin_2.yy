{
    "id": "c380d220-28cf-486b-9346-6ac6b45e5a9b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_food_origin_2",
    "eventList": [
        {
            "id": "28bc1394-aba8-468f-85fa-f66447c20c01",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c380d220-28cf-486b-9346-6ac6b45e5a9b"
        },
        {
            "id": "0cbdb8a7-0d2e-42af-8f9a-dcf9dc54733a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c380d220-28cf-486b-9346-6ac6b45e5a9b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8b4bb6f5-e6de-4e59-b1e3-0cacc6f9329d",
    "visible": true
}