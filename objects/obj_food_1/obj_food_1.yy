{
    "id": "21ec9a0d-6114-4232-abb4-ed8d3beb3fee",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_food_1",
    "eventList": [
        {
            "id": "0b2eba2b-2859-4786-8769-7b2a1b43ba2a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "21ec9a0d-6114-4232-abb4-ed8d3beb3fee"
        },
        {
            "id": "8c3eee4c-dd05-4db7-b479-d79fbf64cd42",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "21ec9a0d-6114-4232-abb4-ed8d3beb3fee"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "aafc3e6f-ee39-4d2d-847e-c2e29d38d05a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "20e3606a-8dca-4535-a0a1-de86a04ddb23",
    "visible": true
}