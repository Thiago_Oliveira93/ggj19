{
    "id": "5f453f5a-2b69-4449-abcc-3aa1e01112fd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_food_origin_3",
    "eventList": [
        {
            "id": "2ded1aef-c4f3-49bf-92a2-b882c4db2b25",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5f453f5a-2b69-4449-abcc-3aa1e01112fd"
        },
        {
            "id": "503361f1-78c5-495e-b2e7-41794114c77d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5f453f5a-2b69-4449-abcc-3aa1e01112fd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "962d5e1b-732b-4877-b853-91c568ee04c3",
    "visible": true
}