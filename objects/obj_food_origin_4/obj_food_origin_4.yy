{
    "id": "6ef7efdc-125a-4b88-8c01-4e86408de258",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_food_origin_4",
    "eventList": [
        {
            "id": "c22405d8-b5f1-471f-a076-1866efa528f3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6ef7efdc-125a-4b88-8c01-4e86408de258"
        },
        {
            "id": "9bae5b5f-d722-417c-84c8-1d8de5ae6872",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6ef7efdc-125a-4b88-8c01-4e86408de258"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d850d4e4-240d-4ae4-880d-6fc84a96fc62",
    "visible": true
}