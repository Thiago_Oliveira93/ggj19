{
    "id": "c149f667-a6bc-457f-8247-f00a9558d8bf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_plate",
    "eventList": [
        {
            "id": "42ea39f4-edf7-4310-8c78-09050dc0f926",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c149f667-a6bc-457f-8247-f00a9558d8bf"
        },
        {
            "id": "6962ddb4-3894-4054-a1ab-feaf3511fc86",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "616ec5b7-fe1d-4c85-a566-715469f7fe9e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c149f667-a6bc-457f-8247-f00a9558d8bf"
        },
        {
            "id": "a72c24ca-e8e4-4e3d-96c0-4bcf6c72c6b0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c149f667-a6bc-457f-8247-f00a9558d8bf"
        },
        {
            "id": "ce398ca6-8b8e-46a3-aa5f-d0b53f60a8bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c149f667-a6bc-457f-8247-f00a9558d8bf"
        },
        {
            "id": "37665d9a-347f-4b44-a30a-b8bdc35731cf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d1dcb5fb-387a-4253-a1f7-27c3c2e3d7ab",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c149f667-a6bc-457f-8247-f00a9558d8bf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "86dce11c-7450-474e-ba0e-2840df79fca6",
    "visible": true
}