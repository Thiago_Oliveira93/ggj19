{
    "id": "5a77d663-ab9c-4df4-ae21-e9a27e7c1a46",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_oven",
    "eventList": [
        {
            "id": "40ce4d02-e7eb-40cd-baa2-cf19ddfe389f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5a77d663-ab9c-4df4-ae21-e9a27e7c1a46"
        },
        {
            "id": "e0e08f41-bf1b-4368-8f26-a198e3fd701f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5a77d663-ab9c-4df4-ae21-e9a27e7c1a46"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d92c4b81-2d90-42d7-81a1-25f96aebd577",
    "visible": false
}