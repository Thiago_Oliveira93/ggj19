{
    "id": "df80c802-06f6-4882-bfad-86e45f1d2c6d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_food_3",
    "eventList": [
        {
            "id": "357b9c76-5085-446d-93ba-5655f5e05b29",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "df80c802-06f6-4882-bfad-86e45f1d2c6d"
        },
        {
            "id": "b84e0d95-f1ab-4cef-8f11-135215e559b1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "df80c802-06f6-4882-bfad-86e45f1d2c6d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "aafc3e6f-ee39-4d2d-847e-c2e29d38d05a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ca9dbcf6-0dad-44ca-b164-ff6b61bb64f3",
    "visible": true
}