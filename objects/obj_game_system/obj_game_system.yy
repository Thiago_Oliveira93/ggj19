{
    "id": "235a07b5-9479-4710-9ece-839032927157",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_game_system",
    "eventList": [
        {
            "id": "f904e714-a9b0-4c80-af19-df7fdf789e83",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "235a07b5-9479-4710-9ece-839032927157"
        },
        {
            "id": "0cba3f91-d234-41e6-9be2-5ec5cd495b65",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "235a07b5-9479-4710-9ece-839032927157"
        },
        {
            "id": "96790a7e-d944-4301-8ef7-79797e55aabc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "235a07b5-9479-4710-9ece-839032927157"
        },
        {
            "id": "6ed3211b-6692-4da8-9cee-70fcedf7dbd7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "235a07b5-9479-4710-9ece-839032927157"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d92c4b81-2d90-42d7-81a1-25f96aebd577",
    "visible": true
}