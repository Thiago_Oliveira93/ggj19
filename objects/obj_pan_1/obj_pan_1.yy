{
    "id": "616ec5b7-fe1d-4c85-a566-715469f7fe9e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_pan_1",
    "eventList": [
        {
            "id": "43a20788-41cf-4949-8a55-b309c49a23ae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "616ec5b7-fe1d-4c85-a566-715469f7fe9e"
        },
        {
            "id": "f2f0b391-4c74-4093-b024-c99eba39b86d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "616ec5b7-fe1d-4c85-a566-715469f7fe9e"
        },
        {
            "id": "51e74448-fa48-42ee-a33f-c8b5c984020c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "616ec5b7-fe1d-4c85-a566-715469f7fe9e"
        },
        {
            "id": "6b1ea6bd-0423-4609-a8fb-aafd49dc28a5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "aafc3e6f-ee39-4d2d-847e-c2e29d38d05a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "616ec5b7-fe1d-4c85-a566-715469f7fe9e"
        },
        {
            "id": "f764424e-8de1-4c5a-91e7-27efdcaf1f6c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "616ec5b7-fe1d-4c85-a566-715469f7fe9e"
        },
        {
            "id": "6869a49d-794e-40a0-94a4-aee046b148ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "c149f667-a6bc-457f-8247-f00a9558d8bf",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "616ec5b7-fe1d-4c85-a566-715469f7fe9e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ef1fd3af-e2a6-4752-9f43-2cc7613fe632",
    "visible": true
}