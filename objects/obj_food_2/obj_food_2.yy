{
    "id": "a28f99d5-99fd-4bae-b76d-0d92658626a4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_food_2",
    "eventList": [
        {
            "id": "3e6a4e06-3a31-46f2-ac09-7f383f1f18e9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a28f99d5-99fd-4bae-b76d-0d92658626a4"
        },
        {
            "id": "3e163132-c167-412c-948a-a54ffeafef96",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a28f99d5-99fd-4bae-b76d-0d92658626a4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "aafc3e6f-ee39-4d2d-847e-c2e29d38d05a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b46bd10e-3fca-4aae-8584-cc5416614e1d",
    "visible": true
}