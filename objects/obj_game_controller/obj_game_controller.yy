{
    "id": "2d5c8d4b-5117-4048-81b0-f189a87bf9b1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_game_controller",
    "eventList": [
        {
            "id": "03602f40-c449-4cb6-a8e5-fa539beddbb1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "2d5c8d4b-5117-4048-81b0-f189a87bf9b1"
        },
        {
            "id": "b0c4133c-ebc3-4286-b0a1-1c3ef7d17b65",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2d5c8d4b-5117-4048-81b0-f189a87bf9b1"
        },
        {
            "id": "233862b7-4c71-4cd9-ae18-8c6588b58abc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2d5c8d4b-5117-4048-81b0-f189a87bf9b1"
        },
        {
            "id": "3e1483f3-490a-45cb-80fa-1e0cfb7798f3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "2d5c8d4b-5117-4048-81b0-f189a87bf9b1"
        },
        {
            "id": "71494b3c-dc0f-4016-85e0-f196af03bfa2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "2d5c8d4b-5117-4048-81b0-f189a87bf9b1"
        },
        {
            "id": "c5e64c05-e123-4780-a1a6-d1c18b129673",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "2d5c8d4b-5117-4048-81b0-f189a87bf9b1"
        },
        {
            "id": "da93f7ed-49b6-41f4-9011-0b09a7ab249e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "2d5c8d4b-5117-4048-81b0-f189a87bf9b1"
        },
        {
            "id": "fc6d2674-3189-47c6-bf4a-21e615864fa0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 2,
            "m_owner": "2d5c8d4b-5117-4048-81b0-f189a87bf9b1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e9fd309e-7555-4e99-8fc2-a5438cc3f016",
    "visible": true
}