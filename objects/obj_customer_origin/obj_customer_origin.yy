{
    "id": "6d91430b-5049-44f8-9d75-9853d64e9071",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_customer_origin",
    "eventList": [
        {
            "id": "6039d268-0ee7-415b-8a63-524c0e2d3d78",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d1dcb5fb-387a-4253-a1f7-27c3c2e3d7ab",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "6d91430b-5049-44f8-9d75-9853d64e9071"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d20162d5-d2b0-4538-84ac-32d62ae6dd3a",
    "visible": true
}