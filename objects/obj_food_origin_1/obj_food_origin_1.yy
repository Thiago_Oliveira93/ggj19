{
    "id": "d3f037a4-5137-414f-84b5-8adda68889d7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_food_origin_1",
    "eventList": [
        {
            "id": "dfd8ecc2-d649-4a13-a67c-63a0b3e0fd06",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d3f037a4-5137-414f-84b5-8adda68889d7"
        },
        {
            "id": "df35cc07-0193-4c31-9070-b7f574c32c88",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d3f037a4-5137-414f-84b5-8adda68889d7"
        },
        {
            "id": "69c50cb1-3605-4b58-8597-4f548f63b583",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d3f037a4-5137-414f-84b5-8adda68889d7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8b353cc7-dfcf-4e2e-8f09-3360a5f70e74",
    "visible": true
}