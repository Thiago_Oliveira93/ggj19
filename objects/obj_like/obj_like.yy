{
    "id": "315a1123-e5cf-4fc2-a454-fc258e012a1c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_like",
    "eventList": [
        {
            "id": "9ed09e36-340e-4cbe-bd0c-369966e92e52",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "315a1123-e5cf-4fc2-a454-fc258e012a1c"
        },
        {
            "id": "7b95c3ae-e419-4db2-9869-d12e80df10bb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "315a1123-e5cf-4fc2-a454-fc258e012a1c"
        },
        {
            "id": "b9db4d4d-2c4c-4f0e-833a-9a35dbd3a1fa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "315a1123-e5cf-4fc2-a454-fc258e012a1c"
        },
        {
            "id": "036c487e-17fc-4fe4-ad1e-843faa7a7a69",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "315a1123-e5cf-4fc2-a454-fc258e012a1c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "749fa1ef-91b8-45f4-a4cb-3f222492959a",
    "visible": true
}