{
    "id": "1afa0abb-2e97-497c-8b5f-169785d2164a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_chef_hand",
    "eventList": [
        {
            "id": "0e7635c4-17f8-47f9-974a-1fc087563573",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1afa0abb-2e97-497c-8b5f-169785d2164a"
        },
        {
            "id": "d298d2d0-8f45-489d-808d-0c977ff85962",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1afa0abb-2e97-497c-8b5f-169785d2164a"
        },
        {
            "id": "d831f91a-4a51-475a-ae67-678e93f0183b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "1afa0abb-2e97-497c-8b5f-169785d2164a"
        },
        {
            "id": "a7adc48b-9973-469c-bae0-3eb2fdcbaff6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "1afa0abb-2e97-497c-8b5f-169785d2164a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d92c4b81-2d90-42d7-81a1-25f96aebd577",
    "visible": true
}